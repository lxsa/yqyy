<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:82:"D:\phpStudy\PHPTutorial\WWW\yqyy\public/../application/index\view\index\index.html";i:1537706007;}*/ ?>
﻿<!doctype html>
<html class="no-js" lang="zxx">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>语桥英语</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Favicons -->
	<link rel="shortcut icon" href="/yqyy/public/static/index/images/favicon.ico">
	<link rel="apple-touch-icon" href="/yqyy/public/static/index/images/icon.png">
	<!-- Google font (font-family: 'Dosis', Roboto;) -->
	<link href="https://fonts.googleapis.com/css?family=Dosis:400,500,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
	 

	<!-- Stylesheets -->
	<link rel="stylesheet" href="/yqyy/public/static/index/css/bootstrap.min.css">
	<link rel="stylesheet" href="/yqyy/public/static/index/css/plugins.css">
	<link rel="stylesheet" href="/yqyy/public/static/index/style.css">

	<!-- Cusom css -->
   <link rel="stylesheet" href="/yqyy/public/static/index/css/custom.css">

	<!-- Modernizer js -->
	<script src="/yqyy/public/static/index/js/vendor/modernizr-3.5.0.min.js"></script>
</head>
<body>
	<!--[if lte IE 9]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
	<![endif]-->

	<!-- Add your site or application content here -->
	
	<!-- <div class="fakeloader"></div> -->

	<!-- Main wrapper -->
	<div class="wrapper" id="wrapper">
		<!-- Header -->
		<header id="header" class="jnr__header header--one clearfix">
			<!-- Start Header Top Area -->
			<div class="junior__header__top">
				<div class="container">
					<div class="row">
						<div class="col-md-7 col-lg-6 col-sm-12">
							<div class="jun__header__top__left">
								<ul class="top__address d-flex justify-content-start align-items-center flex-wrap flex-lg-nowrap flex-md-nowrap">
									<li><i class="fa fa-envelope"></i><a href="#">88354666@qq.com</a></li>
									<li><i class="fa fa-phone"></i><span>Phone :</span><a href="#">13636625178</a></li>
								</ul>
							</div>
						</div>
						<div class="col-md-5 col-lg-6 col-sm-12">
							<div class="jun__header__top__right">
								<ul class="accounting d-flex justify-content-lg-end justify-content-md-end justify-content-start align-items-center">
									<li><a class="login-trigger" href="#">登入</a></li>
									<li><a class="accountbox-trigger" href="#">注册</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Header Top Area -->
			<!-- Start Mainmenu Area -->
			<div class="mainmenu__wrapper bg__cat--1 poss-relative header_top_line sticky__header">
				<div class="container">
					<div class="row d-none d-lg-flex">
						<div class="col-sm-4 col-md-6 col-lg-2 order-1 order-lg-1">
							<div class="logo">
								<a href="index.html">
									<img src="/yqyy/public/static/index/images/logo/junior.png" width="90" alt="logo images ">
								</a>
							</div>
						</div>
						<div class="col-sm-4 col-md-2 col-lg-9 order-3 order-lg-2">
							<div class="mainmenu__wrap">
								<nav class="mainmenu__nav">
                                    <ul class="mainmenu" >
                                        <li><a href="index.html">语桥英语</a></li>
                                        <li><a href="#">教学成果</a></li>
										<li><a href="#">精彩活动</a></li>
										<li><a href="#">师资保障</a></li>
                                        <li><a href="#">关于我们</a></li>
                                    </ul>
                                </nav>
							</div>
						</div>

					</div>
					<!-- Mobile Menu -->
                    <div class="mobile-menu d-block d-lg-none">
                    	<div class="logo">
                    		<a href="index.html"><img src="/yqyy/public/static/index/images/logo/junior.png" width="50" alt="logo"></a>
                    	</div>
                    	
                    </div>
                    <!-- Mobile Menu -->
				</div>
			</div>
			<!-- End Mainmenu Area -->
		</header>
		<!-- //Header -->
		<!-- Strat Slider Area -->
		<div class="slide__carosel owl-carousel owl-theme">
			<div class="slider__area bg-pngimage--1  d-flex fullscreen justify-content-start align-items-center">
				<div class="container">
					<div class="row">
						<div class="col-md-12 col-lg-12 col-sm-12">
							<div class="slider__activation">
								<!-- Start Single Slide -->
								<div class="slide">
									<div class="slide__inner">
										<h1>Now! & 加入我们</h1>
										<div class="slider__text">
											<h2>分享学习的快乐</h2>
										</div>
										<div class="slider__btn">
											<a class="dcare__btn" href="#">点击加入</a>
										</div>
									</div>
								</div>
								<!-- End Single Slide -->
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="slider__area bg-pngimage--1  d-flex fullscreen justify-content-start align-items-center">
				<div class="container">
					<div class="row">
						<div class="col-md-12 col-lg-12 col-sm-12">
							<div class="slider__activation">
								<!-- Start Single Slide -->
								<div class="slide">
									<div class="slide__inner">
										<h1>Play & learn How to</h1>
										<div class="slider__text">
											<h2>Creat New Things</h2>
										</div>
										<div class="slider__btn">
											<a class="dcare__btn" href="#">READ MORE</a>
										</div>
									</div>
								</div>
								<!-- End Single Slide -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Slider Area -->

		<!-- Start Welcame Area -->
		<section class="junior__welcome__area section-padding--md bg-pngimage--2">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="section__title text-center">
							<h2 class="title__line">欢迎加入我们的大家庭</h2>
							<p>全程外教授课，经验丰富的中教，全程跟踪，浸入式美语欢乐课堂，教学互动、体验活动，让孩子感受纯正的美式课堂。</p>
						</div>
					</div>
				</div>
				<div class="row jn__welcome__wrapper align-items-center">
					<div class="col-md-12 col-lg-6 col-sm-12">
						<div class="welcome__juniro__inner">
							<h3>Welcome to Our School</h3>
							<p>根据Listening，Phonics， Reading的母语教学理念, 我们遵从孩子的身心发展和认知习惯，提高听说读写的语言运用能力，让学英语像学母语一样简单。我们倡导以21世纪技能为主的全人发展， 培养具备：生活和职业规划能力、学习和创新能力、信息、媒体和技术能力这三大方面技能的21世纪国际公民。</p>
							<div class="wel__btn">
								<a class="dcare__btn" href="#">Read More</a>
							</div>
						</div>
					</div>
					<div class="col-md-12 col-lg-6 col-sm-12 md-mt-40 sm-mt-40">
						<div class="jnr__Welcome__thumb">
							<img src="/yqyy/public/static/index/images/wel-come/1.png" alt="images">
							<a class="play__btn" href="#"><i class="fa fa-play"></i></a>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End Welcame Area -->

		<!-- Start Our Service Area -->
		<section class="junior__service bg-image--1 section-padding--bottom section--padding--xlg--top">
			<div class="container">
				<div class="row">
					<!-- Start Single Service -->
					<div class="col-lg-3 col-md-6 col-sm-6 col-12">
						<div class="service bg--white border__color wow fadeInUp">
							<div class="service__icon">
								<img src="/yqyy/public/static/index/images/shape/sm-icon/1.png" alt="icon images">
							</div>
							<div class="service__details">
								<h6><a href="#">课程列表</a></h6>
								<p>在经过多年的应试教育后，英语口语成为广大英语学习者最为薄弱的环节。英孚的英语课程旨在帮助孩子解决英语口语的困扰。我们不仅关注学员的英语基础学习，更注重孩子语言运用技能的培养，帮助孩子自信开口说英语。</p>
								<div class="service__btn">
									<a class="dcare__btn--2" href="#">Read More</a>
								</div>
							</div>
						</div>
					</div>
					<!-- End Single Service -->
					<!-- Start Single Service -->
					<div class="col-lg-3 col-md-6 col-sm-6 col-12 xs-mt-60">
						<div class="service bg--white border__color border__color--2 wow fadeInUp" data-wow-delay="0.2s">
							<div class="service__icon">
								<img src="/yqyy/public/static/index/images/shape/sm-icon/2.png" alt="icon images">
							</div>
							<div class="service__details">
								<h6><a href="#">主动学习</a></h6>
								<p>不同年龄段的孩子对学习有着多样的需求。我们的课程针对各年龄孩子的不同需求量身定制英语课程：采用针对性的教材、教学方法，设定不同的教学目标。每一个孩子在性格、喜好和学习方式上各有不同。我们的英语培训师密切关注每一位学员的特点，并根据学员的个性特征帮助孩子取得学习和生活上的成功。</p>
								<div class="service__btn">
									<a class="dcare__btn--2" href="#">Read More</a>
								</div>
							</div>
						</div>
					</div>
					<!-- End Single Service -->
					<!-- Start Single Service -->
					<div class="col-lg-3 col-md-6 col-sm-6 col-12 md-mt-60 sm-mt-60">
						<div class="service bg--white border__color border__color--3 wow fadeInUp" data-wow-delay="0.45s">
							<div class="service__icon">
								<img src="/yqyy/public/static/index/images/shape/sm-icon/3.png" alt="icon images">
							</div>
							<div class="service__details">
								<h6><a href="#">创造性培养</a></h6>
								<p>首先，家长应该鼓励孩子多观察。观察是孩子认识事物的基础，通过细致入微的观察，孩子才能找到创新的灵感。瓦特发明蒸汽机的故事，可谓家喻户晓，他观察到水烧开时，蒸汽就不断地往上冒，推动壶盖不停地跳动，好象里边藏着个魔术师，在变戏法似的，他从中得到灵感，从而发明了蒸汽机，推动了第一次工业革命的开展，人类社会进入了蒸汽时代。</p>
								<div class="service__btn">
									<a class="dcare__btn--2" href="#">Read More</a>
								</div>
							</div>
						</div>
					</div>
					<!-- End Single Service -->
					<!-- Start Single Service -->
					<div class="col-lg-3 col-md-6 col-sm-6 col-12 md-mt-60 sm-mt-60">
						<div class="service bg--white border__color border__color--4 wow fadeInUp" data-wow-delay="0.65s">
							<div class="service__icon">
								<img src="/yqyy/public/static/index/images/shape/sm-icon/4.png" alt="icon images">
							</div>
							<div class="service__details">
								<h6><a href="#">拓展实践</a></h6>
								<p>为孩子们挑选合适的英语培训师：他们不仅拥有优秀的英语教学资质，更有热情开朗的外向型性格，才能尽可能调动孩子的积极性，让孩子在轻松欢乐的气氛中学习英语。 英孚聘用的外籍英语培训师大多数来自以英语为母语的国家，并持有国际教学资格认证。在录用之后，他们还会接受系统的内部培训，以适应不同的课程教学。</p>
								<div class="service__btn">
									<a class="dcare__btn--2" href="#">Read More</a>
								</div>
							</div>
						</div>
					</div>
					<!-- End Single Service -->
				</div>
			</div>
		</section>
		<!-- End Our Service Area -->

		<!-- Start Call To Action -->
		<section class="jnr__call__to__action bg-pngimage--3">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-sm-12 col-md-12">
						<div class="jnr__call__action__wrap d-flex flex-wrap flex-md-nowrap flex-lg-nowrap justify-content-between align-items-center">
							<div class="callto__action__inner">
								<h2>如何让您的孩子尽快融入一个集体 ?</h2>
								<p>在这个充满创新和挑战的时代，只要我们遵循正确的学习理念和路径，
									每个孩子都能做到‘Speak Like American Born’的21世纪国际小公民！  </p>
							</div>
							<div class="callto__action__btn">
								<a class="dcare__btn btn__white" href="#">Read More</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End Call To Action -->

		<!-- Start our Class Area -->
		<section class="junior__classes__area section-lg-padding--top section-padding--md--bottom bg--white">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-lg-12 col-sm-12">
						<div class="section__title text-center">
							<h2 class="title__line">选择您的私人定制课程</h2>
							<p>根据一对一测评结果，选择不同级别的主修课程，3-6岁幼儿英语探索课程是一项专为幼儿设计的基础性英语课程。课程旨在培养孩子良好的语感，提高社交能力、精细动作和认知能力。我们根据中国孩子不同的特点因材施教，交际教学法、全身反应教学法、故事教学法、任务教学法等多元化的教学手法，将牢牢抓住3-6岁孩子的注意力，帮助孩子高效学英语</p>
						</div>
					</div>
				</div>
				<div class="row classes__wrap activation__one owl-carousel owl-theme clearfix mt--40">
					<!-- Start Single Classes -->
					<div class="col-lg-4 col-sm-6">
						<div class="junior__classes">
							<div class="classes__thumb">
								<a href="#">
									<img src="/yqyy/public/static/index/images/class/md-img/1.jpg" alt="class images">
								</a>
							</div>
							<div class="classes__inner">
								<div class="classes__icon">
									<img src="/yqyy/public/static/index/images/class/star/1.png" alt="starr images">
									<span>暂定</span>
								</div>
								<div class="class__details">
									<h4><a href="#">儿童英语（3-9岁）</a></h4>
									<ul class="class__time">
										<li>学习时长 : 0.8 - 2.5 Years</li>
										<li>课程 : 8</li>
									</ul>
									<div class="class__btn">
										<a class="dcare__btn--2" href="#">点击了解</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- End Single Classes -->
					<!-- Start Single Classes -->
					<div class="col-lg-4 col-sm-6">
						<div class="junior__classes">
							<div class="classes__thumb">
								<a href="#">
									<img src="/yqyy/public/static/index/images/class/md-img/2.jpg" alt="class images">
								</a>
							</div>
							<div class="classes__inner">
								<div class="classes__icon">
									<img src="/yqyy/public/static/index/images/class/star/1.png" alt="starr images">
									<span>暂定</span>
								</div>
								<div class="class__details">
									<h4><a href="#">幼儿英语探索课程（3-6岁） </a></h4>
									<ul class="class__time">
										<li>学习时长 : 0.8 - 2.5 Years</li>
										<li>课程 : 8</li>
									</ul>
									<div class="class__btn">
										<a class="dcare__btn--2" href="#">点击了解</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- End Single Classes -->
					<!-- Start Single Classes -->
					<div class="col-lg-4 col-sm-6">
						<div class="junior__classes">
							<div class="classes__thumb">
								<a href="#">
									<img src="/yqyy/public/static/index/images/class/md-img/3.jpg" alt="class images">
								</a>
							</div>
							<div class="classes__inner">
								<div class="classes__icon">
									<img src="/yqyy/public/static/index/images/class/star/1.png" alt="starr images">
									<span>暂定</span>
								</div>
								<div class="class__details">
									<h4><a href="#">青少儿英语（10-18岁） </a></h4>
									<ul class="class__time">
										<li>学习时长 : 0.8 - 2.5 Years</li>
										<li>课程 : 8</li>
									</ul>
									<div class="class__btn">
										<a class="dcare__btn--2" href="#">点击了解</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- End Single Classes -->
					<!-- Start Single Classes -->
					<div class="col-lg-4 col-sm-6">
						<div class="junior__classes">
							<div class="classes__thumb">
								<a href="#">
									<img src="/yqyy/public/static/index/images/class/md-img/3.jpg" alt="class images">
								</a>
							</div>
							<div class="classes__inner">
								<div class="classes__icon">
									<img src="/yqyy/public/static/index/images/class/star/1.png" alt="starr images">
									<span>$50</span>
								</div>
								<div class="class__details">
									<h4><a href="#">Swimming Class</a></h4>
									<ul class="class__time">
										<li>Infant Care : 0.8 - 2.5 Years</li>
										<li>Class Size : 8</li>
									</ul>
									<div class="class__btn">
										<a class="dcare__btn--2" href="#">Admission Now</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- End Single Classes -->
					<!-- Start Single Classes -->
					<div class="col-lg-4 col-sm-6">
						<div class="junior__classes">
							<div class="classes__thumb">
								<a href="#">
									<img src="/yqyy/public/static/index/images/class/md-img/1.jpg" alt="class images">
								</a>
							</div>
							<div class="classes__inner">
								<div class="classes__icon">
									<img src="/yqyy/public/static/index/images/class/star/1.png" alt="starr images">
									<span>$60</span>
								</div>
								<div class="class__details">
									<h4><a href="#">Swimming Class</a></h4>
									<ul class="class__time">
										<li>Infant Care : 0.8 - 2.5 Years</li>
										<li>Class Size : 8</li>
									</ul>
									<div class="class__btn">
										<a class="dcare__btn--2" href="#">Admission Now</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- End Single Classes -->
				</div>
			</div>
		</section>
		<!-- End our Class Area -->
		
		<!-- Start Testimonial Area -->
		<section class="junior__testimonial__area bg-image--2 section-padding--lg">
			<div class="container">
				<div class="row">
					<div class="offset-lg-2 col-lg-8 col-md-12 col-sm-12">
						<div class="testimonial__container">
							<div class="tes__activation--1 owl-carousel owl-theme">
								<div class="testimonial__bg">
									<!-- Start Single Testimonial -->
									<div class="testimonial text-center">
										<div class="testimonial__inner">
											<div class="test__icon">
												<img src="/yqyy/public/static/index/images/testimonial/icon/1.png" alt="icon images">
											</div>
											<div class="client__details">
												<p>致语桥英语：
													在语桥英语学习的这段时间，Ioannes每次都很期待去上课，课堂上老师形象生动的肢体表现结合美式发音，能让孩子快速记住单词且记忆深刻，为将来更好的生活打下坚实的基础，感谢！ </p>
												<div class="client__info">
													<h6>Ioannes妈妈</h6>
													<span>language bridge of student</span>
												</div>
											</div>
										</div>
									</div>
									<!-- End Single Testimonial -->
								</div>
								<div class="testimonial__bg">
									<!-- Start Single Testimonial -->
									<div class="testimonial text-center">
										<div class="testimonial__inner">
											<div class="test__icon">
												<img src="/yqyy/public/static/index/images/testimonial/icon/1.png" alt="icon images">
											</div>
											<div class="client__details">
												<p>后在这里的时光课堂上我们学会大胆自信的表达、积极主动配合每项活动、生活中不断去探索新的事物、半年过去后，妈妈从你的身上看到了各方均衡成长，不管是视野的拓宽还是思维能力提升</p>
												<div class="client__info">
													<h6> alica</h6>
													<span>language bridge of student</span>
												</div>
											</div>
										</div>
									</div>
									<!-- End Single Testimonial -->
								</div>
								<div class="testimonial__bg">
									<!-- Start Single Testimonial -->
									<div class="testimonial text-center">
										<div class="testimonial__inner">
											<div class="test__icon">
												<img src="/yqyy/public/static/index/images/testimonial/icon/1.png" alt="icon images">
											</div>
											<div class="client__details">
												<p>第一次走进爱贝国际少儿英语时，你羞涩的躲在妈妈身后，不够自信的和老师Say Hello。之后在这里的时光课堂上我们学会大胆自信的表达、不管是视野的拓宽还是思维能力提升。幸运能与爱贝相遇、感谢爱贝老师们辛勤付出。
			</p>
												<div class="client__info">
													<h6>Amy妈妈</h6>
													<span>language bridge of student</span>
												</div>
											</div>
										</div>
									</div>
									<!-- End Single Testimonial -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End Testimonial Area -->
		<!-- Start Our Gallery Area -->
		<section class="junior__gallery__area bg--white section-padding--lg">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-sm-12 col-md-12">
						<div class="section__title text-center">
							<h2 class="title__line">明日之星</h2>
							<p>我们关注孩子的下一个十年，在这个充满创新和挑战的时代，只要我们遵循正确的学习理念和路径，
								每个孩子都能做到‘Speak Like American Born’的21世纪国际小公民</p>
						</div>
					</div>
				</div>
				<div class="row galler__wrap mt--40">
					<!-- Start Single Gallery -->
					<div class="col-lg-4 col-md-6 col-sm-6 col-12">
						<div class="gallery wow fadeInUp">
							<div class="gallery__thumb">
								<a href="#">
									<img src="/yqyy/public/static/index/images/gallery/gallery-1/1.jpg" alt="gallery images">
								</a>
							</div>
							<div class="gallery__hover__inner">
								<div class="gallery__hover__action">
									<ul class="gallery__zoom">
										<li><a href="/yqyy/public/static/index/images/gallery/big-img/1.jpg" data-lightbox="grportimg" data-title="My caption"><i class="fa fa-search"></i></a></li>
										<li><a href="#"><i class="fa fa-link"></i></a></li>
									</ul>
									<h4 class="gallery__title"><a href="#">美好时光</a></h4>
								</div>
							</div>
						</div>	
					</div>	
					<!-- End Single Gallery -->
					<!-- Start Single Gallery -->
					<div class="col-lg-4 col-md-6 col-sm-6 col-12">
						<div class="gallery wow fadeInUp">
							<div class="gallery__thumb">
								<a href="#">
									<img src="/yqyy/public/static/index/images/gallery/gallery-1/2.jpg" alt="gallery images">
								</a>
							</div>
							<div class="gallery__hover__inner">
								<div class="gallery__hover__action">
									<ul class="gallery__zoom">
										<li><a href="/yqyy/public/static/index/images/gallery/big-img/2.jpg" data-lightbox="grportimg" data-title="My caption"><i class="fa fa-search"></i></a></li>
										<li><a href="#"><i class="fa fa-link"></i></a></li>
									</ul>
									<h4 class="gallery__title"><a href="#">美好时光</a></h4>
								</div>
							</div>
						</div>	
					</div>	
					<!-- End Single Gallery -->
					<!-- Start Single Gallery -->
					<div class="col-lg-4 col-md-6 col-sm-6 col-12">
						<div class="gallery wow fadeInUp">
							<div class="gallery__thumb">
								<a href="#">
									<img src="/yqyy/public/static/index/images/gallery/gallery-1/3.jpg" alt="gallery images">
								</a>
							</div>
							<div class="gallery__hover__inner">
								<div class="gallery__hover__action">
									<ul class="gallery__zoom">
										<li><a href="/yqyy/public/static/index/images/gallery/big-img/3.jpg" data-lightbox="grportimg" data-title="My caption"><i class="fa fa-search"></i></a></li>
										<li><a href="#"><i class="fa fa-link"></i></a></li>
									</ul>
									<h4 class="gallery__title"><a href="#">美好时光</a></h4>
								</div>
							</div>
						</div>	
					</div>	
					<!-- End Single Gallery -->
					<!-- Start Single Gallery -->
					<div class="col-lg-4 col-md-6 col-sm-6 col-12">
						<div class="gallery wow fadeInUp">
							<div class="gallery__thumb">
								<a href="#">
									<img src="/yqyy/public/static/index/images/gallery/gallery-1/4.jpg" alt="gallery images">
								</a>
							</div>
							<div class="gallery__hover__inner">
								<div class="gallery__hover__action">
									<ul class="gallery__zoom">
										<li><a href="/yqyy/public/static/index/images/gallery/big-img/4.jpg" data-lightbox="grportimg" data-title="My caption"><i class="fa fa-search"></i></a></li>
										<li><a href="#"><i class="fa fa-link"></i></a></li>
									</ul>
									<h4 class="gallery__title"><a href="#">美好时光</a></h4>
								</div>
							</div>
						</div>	
					</div>	
					<!-- End Single Gallery -->
					<!-- Start Single Gallery -->
					<div class="col-lg-4 col-md-6 col-sm-6 col-12">
						<div class="gallery wow fadeInUp">
							<div class="gallery__thumb">
								<a href="#">
									<img src="/yqyy/public/static/index/images/gallery/gallery-1/5.jpg" alt="gallery images">
								</a>
							</div>
							<div class="gallery__hover__inner">
								<div class="gallery__hover__action">
									<ul class="gallery__zoom">
										<li><a href="/yqyy/public/static/index/images/gallery/big-img/5.jpg" data-lightbox="grportimg" data-title="My caption"><i class="fa fa-search"></i></a></li>
										<li><a href="#"><i class="fa fa-link"></i></a></li>
									</ul>
									<h4 class="gallery__title"><a href="#">美好时光</a></h4>
								</div>
							</div>
						</div>	
					</div>	
					<!-- End Single Gallery -->
					<!-- Start Single Gallery -->
					<div class="col-lg-4 col-md-6 col-sm-6 col-12">
						<div class="gallery wow fadeInUp">
							<div class="gallery__thumb">
								<a href="#">
									<img src="/yqyy/public/static/index/images/gallery/gallery-1/6.jpg" alt="gallery images">
								</a>
							</div>
							<div class="gallery__hover__inner">
								<div class="gallery__hover__action">
									<ul class="gallery__zoom">
										<li><a href="/yqyy/public/static/index/images/gallery/big-img/6.jpg" data-lightbox="grportimg" data-title="My caption"><i class="fa fa-search"></i></a></li>
										<li><a href="#"><i class="fa fa-link"></i></a></li>
									</ul>
									<h4 class="gallery__title"><a href="#">美好时光</a></h4>
								</div>
							</div>
						</div>	
					</div>	
					<!-- End Single Gallery -->
				</div>	
			</div>
		</section>
		<!-- End Our Gallery Area -->
		<!-- Start Blog Area -->
		<section class="jnr__blog_area section-padding--lg bg-image--3">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-sm-12 col-md-12">
						<div class="section__title text-center white--title">
							<h2 class="title__line">明星学员</h2>
							<p>明星学员明星学员明星学员明星学员明星学员</p>
						</div>
					</div>
				</div>
				<div class="row blog__wrapper mt--40">
					<!-- Start Single Blog -->
					<div class="col-lg-4 col-md-6 col-sm-12">
						<article class="blog">
							<div class="blog__date">
								<span>添加时间 : 2018年5月25日</span>
							</div>
							<div class="blog__thumb">
								<a href="#">
									<img src="/yqyy/public/static/index/images/blog/md-img/1.jpg" alt="blog images">
								</a>
							</div>
							<div class="blog__inner">
								<span>学员 : 小明</span>
								<h4><a href="#">明星学员明星学员明星学员明星学员</a></h4>
								<p>明星学员明星学员明星学员明星学员明星学员明星学员</p>
								<ul class="blog__meta d-flex flex-wrap flex-md-nowrap flex-lg-nowrap justify-content-between">
									<li><a href="#">评论 : 05</a></li>
									<li><a href="#">喜欢 : 07</a></li>
								</ul>
							</div>
						</article>
					</div>
					<!-- End Single Blog -->
					<!-- Start Single Blog -->
					<div class="col-lg-4 col-md-6 col-sm-12">
						<article class="blog">
							<div class="blog__date">
								<span>添加时间 : 2018年5月25日</span>
							</div>
							<div class="blog__thumb">
								<a href="#">
									<img src="/yqyy/public/static/index/images/blog/md-img/2.jpg" alt="blog images">
								</a>
							</div>
							<div class="blog__inner">
								<span>明星学员</span>
								<h4><a href="#">明星学员明星学员明星学员</a></h4>
								<p>明星学员明星学员明星学员明星学员明星学员明星学员明星学员</p>
								<ul class="blog__meta d-flex flex-wrap flex-md-nowrap flex-lg-nowrap justify-content-between">
									<li><a href="#">评论 : 05</a></li>
									<li><a href="#">喜欢 : 07</a></li>
								</ul>
							</div>
						</article>
					</div>
					<!-- End Single Blog -->
					<!-- Start Single Blog -->
					<div class="col-lg-4 col-md-6 col-sm-12">
						<article class="blog">
							<div class="blog__date">
								<span>添加时间 : 2018年5月25日</span>
							</div>
							<div class="blog__thumb">
								<a href="#">
									<img src="/yqyy/public/static/index/images/blog/md-img/3.jpg" alt="blog images">
								</a>
							</div>
							<div class="blog__inner">
								<span>明星学员</span>
								<h4><a href="#">明星学员明星学员</a></h4>
								<p>明星学员明星学员明星学员明星学员明星学员明星学员明星学员明星学员</p>
								<ul class="blog__meta d-flex flex-wrap flex-md-nowrap flex-lg-nowrap justify-content-between">
									<li><a href="#">评论 : 05</a></li>
									<li><a href="#">喜欢 : 07</a></li>
								</ul>
							</div>
						</article>
					</div>
					<!-- End Single Blog -->
				</div>
			</div>
		</section>
		<!-- End Blog Area -->
		<!-- Start upcomming Area -->
		<section class="junior__upcomming__area section-padding--lg bg--white">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-sm-12 col-md-12">
						<div class="section__title text-center">
							<h2 class="title__line">近期活动</h2>
							<p>一个良好的学习环境是孩子成功的动力</p>
						</div>
					</div>
				</div>
				<div class="row upcomming__wrap mt--40">
					<!-- Start Single Upcomming Event -->
					<div class="col-lg-6 col-md-12 col-sm-12">
						<div class="upcomming__event">
							<div class="upcomming__thumb">
								<img src="/yqyy/public/static/index/images/upcomming/1.png" alt="upcomming images">
							</div>
							<div class="upcomming__inner">
								<h6><a href="#">英语沙龙</a></h6>
								<p>英语沙龙英语沙龙英语沙龙英语沙龙英语沙龙英语沙龙英语沙龙英语沙龙英语沙龙英语沙龙英语沙龙英语沙龙英语沙龙英语沙龙</p>
								<ul class="event__time">
									<li><i class="fa fa-home"></i>新场 冠郡中心<br>新场 汇锦城中心 </li>
									<li><i class="fa fa-clock-o"></i>10.00 am to 5.00 pm</li>
								</ul>
							</div>
							<div class="event__occur">
								<img src="/yqyy/public/static/index/images/upcomming/shape/1.png" alt="shape images">
								<div class="enent__pub">
									<span class="time">1日 </span>
									<span class="bate">6月,2018</span>
								</div>
							</div>
						</div>
					</div>
					<!-- End Single Upcomming Event -->
					<!-- Start Single Upcomming Event -->
					<div class="col-lg-6 col-md-12 col-sm-12">
						<div class="upcomming__event">
							<div class="upcomming__thumb">
								<img src="/yqyy/public/static/index/images/upcomming/2.png" alt="upcomming images">
							</div>
							<div class="upcomming__inner">
								<h6><a href="#">语桥英语庆六一赠券回馈活动</a></h6>
								<p>新老学员适用
									转发集60个赞，获800元优惠券
									转发集50个赞，获500元优惠券
									转发集40个赞，获300元优惠券
									活动截止至2018年6月1日24时</p>
								<ul class="event__time">
									<li><i class="fa fa-home"></i>新场 冠郡中心<br>新场 汇锦城中心 </li>
									<li><i class="fa fa-clock-o"></i>10.00 am to 5.00 pm</li>
								</ul>
							</div>
							<div class="event__occur">
								<img src="/yqyy/public/static/index/images/upcomming/shape/1.png" alt="shape images">
								<div class="enent__pub">
									<span class="time">30日 </span>
									<span class="bate">5月,2018</span>
								</div>
							</div>
						</div>
					</div>
					<!-- End Single Upcomming Event -->
				</div>
			</div>
		</section>
		<!-- End upcomming Area -->
		<!-- Start Subscribe Area -->
		<!--<section class="bcare__subscribe subscribe&#45;&#45;1">-->
			<!--<div class="container bg__cat&#45;&#45;3">-->
				<!--<div class="row">-->
					<!--<div class="col-lg-12 col-sm-12 col-lg-12">-->
						<!--<div class="subscribe__inner">-->
							<!--<h2>Subscribe To Our Special Offers</h2>-->
                            <!--<div class="newsletter__form">-->
                                <!--<div class="input__box">-->
                                    <!--<div id="mc_embed_signup">-->
                                        <!--<form action="#" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>-->
                                            <!--<div id="mc_embed_signup_scroll" class="htc__news__inner">-->
                                                <!--<div class="news__input">-->
                                                    <!--<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="Enter Your E-mail" required>-->
                                                <!--</div>-->
                                                <!--&lt;!&ndash; real people should not fill this in and expect good things - do not remove this or risk form bot signups&ndash;&gt;-->
                                                <!--<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_6bbb9b6f5827bd842d9640c82_05d85f18ef" tabindex="-1" value=""></div>-->
                                                <!--<div class="clearfix subscribe__btn"><input class="bst__btn btn&#45;&#45;white__color" type="submit" value="Send Now" name="subscribe" id="mc-embedded-subscribe">-->
                                                <!--</div>-->
                                            <!--</div>-->
                                        <!--</form>-->
                                    <!--</div>-->
                                <!--</div>        -->
                            <!--</div>-->
						<!--</div>-->
					<!--</div>-->
				<!--</div>-->
			<!--</div>-->
		</section>
		<!-- End Subscribe Area -->
		<!-- Footer Area -->
		<footer id="footer" class="footer-area footer--2">
			<div class="footer__wrapper bg-image--10 section-padding--lg">
				<div class="container">
					<div class="row">
						<!-- Start Single Widget -->
						<div class="col-lg-3 col-md-6 col-sm-12">
							<div class="footer__widget">
								<div class="ft__logo">
									<a href="index.html">
										<img src="/yqyy/public/static/index/images/logo/junior.png" alt="logo images">
									</a>
								</div>
								<div class="ftr__details">
									<!--<p>新场 冠郡中心-->
										<!--新场 汇锦城中心 </p>-->
								</div>
								<div class="ftr__address__inner">
									<!--<div class="footer__social__icon">-->
										<!--<ul class="dacre__social__link&#45;&#45;2 d-flex justify-content-start">-->
											<!--<li class="facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>-->
											<!--<li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>-->
											<!--<li class="vimeo"><a href="#"><i class="fa fa-vimeo"></i></a></li>-->
											<!--<li class="pinterest"><a href="#"><i class="fa fa-pinterest-p"></i></a></li>-->
										<!--</ul>-->
									<!--</div>-->
									<!--<div class="ft__btm__title">-->
										<!--<h4>About Us</h4>-->
									<!--</div>-->
								</div>
							</div>
						</div>
						<!-- End Single Widget -->
						<!-- Start Single Widget -->
						<div class="col-lg-4 col-md-6 col-sm-12 sm-mt-40">
							<!--<div class="footer__widget">-->
								<!--<h4>Latest Blog</h4>-->
								<!--<div class="footer__innner">-->
									<!--<div class="ftr__latest__post">-->
										<!--&lt;!&ndash; Start Single &ndash;&gt;-->
										<!--<div class="single__ftr__post d-flex">-->
											<!--<div class="ftr__post__thumb">-->
												<!--<a href="#">-->
													<!--<img src="/yqyy/public/static/index/images/blog/post-img/2.jpg" alt="post images">-->
												<!--</a>-->
											<!--</div>-->
											<!--<div class="ftr__post__details">-->
												<!--<h6><a href="#">Sports Day is near! so lets get ready soon</a></h6>-->
												<!--<span><i class="fa fa-calendar"></i>30th Dec, 2018</span>-->
											<!--</div>-->
										<!--</div>-->
										<!--&lt;!&ndash; End Single &ndash;&gt;-->
										<!--&lt;!&ndash; Start Single &ndash;&gt;-->
										<!--<div class="single__ftr__post d-flex">-->
											<!--<div class="ftr__post__thumb">-->
												<!--<a href="#">-->
													<!--<img src="/yqyy/public/static/index/images/blog/post-img/3.jpg" alt="post images">-->
												<!--</a>-->
											<!--</div>-->
											<!--<div class="ftr__post__details">-->
												<!--<h6><a href="#">Sports Day Celebration</a></h6>-->
												<!--<span><i class="fa fa-calendar"></i>21th Dec, 2018</span>-->
											<!--</div>-->
										<!--</div>-->
										<!--&lt;!&ndash; End Single &ndash;&gt;-->
										<!--&lt;!&ndash; Start Single &ndash;&gt;-->
										<!--<div class="single__ftr__post d-flex">-->
											<!--<div class="ftr__post__thumb">-->
												<!--<a href="#">-->
													<!--<img src="/yqyy/public/static/index/images/blog/post-img/4.jpg" alt="post images">-->
												<!--</a>-->
											<!--</div>-->
											<!--<div class="ftr__post__details">-->
												<!--<h6><a href="#">Sports Day Celebration</a></h6>-->
												<!--<span><i class="fa fa-calendar"></i>10th Dec, 2018</span>-->
											<!--</div>-->
										<!--</div>-->
										<!--&lt;!&ndash; End Single &ndash;&gt;-->
									<!--</div>-->
								<!--</div>-->
							<!--</div>-->
						</div>
						<!-- End Single Widget -->
						<!-- Start Single Wedget -->
						<div class="col-lg-2 col-md-6 col-sm-12 md-mt-40 sm-mt-40">
							<!--<div class="footer__widget">-->
								<!--<h4>Categories</h4>-->
								<!--<div class="footer__innner">-->
									<!--<div class="ftr__latest__post">-->
										<!--<ul class="ftr__catrgory">-->
											<!--<li><a href="#">Painting</a></li>-->
											<!--<li><a href="#">Alphabet Matching</a></li>-->
											<!--<li><a href="#">Drawing</a></li>-->
											<!--<li><a href="#">Swimming</a></li>-->
											<!--<li><a href="#">Sports & Games</a></li>-->
											<!--<li><a href="#">Painting</a></li>-->
											<!--<li><a href="#">Alphabet Matching</a></li>-->
										<!--</ul>-->
									<!--</div>-->
								<!--</div>-->
							<!--</div>-->
						</div>
						<!-- End Single Wedget -->
						<!-- Start Single Widget -->
						<div class="col-lg-3 col-md-6 col-sm-12 md-mt-40 sm-mt-40">
							<!--<div class="footer__widget">-->
								<!--<h4>Twitter Widget</h4>-->
								<!--<div class="footer__innner">-->
									<!--<div class="dcare__twit__wrap">-->
										<!--&lt;!&ndash; Start Single &ndash;&gt;-->
										<!--<div class="dcare__twit d-flex">-->
											<!--<div class="dcare__twit__icon">-->
												<!--<i class="fa fa-twitter"></i>-->
											<!--</div>-->
											<!--<div class="dcare__twit__details">-->
												<!--<p>Lorem ipsum dolor sit  consect ietur adipisicing sed  eiipsa<a href="#"># twitter .com?web/lnk/statement</a></p>-->
												<!--<span><i class="fa fa-clock-o"></i>30th Dec, 2018</span>-->
												<!--<span><i class="fa fa-calendar"></i>30th Dec, 2018</span>-->
											<!--</div>-->
										<!--</div>-->
										<!--&lt;!&ndash; End Single &ndash;&gt;-->
										<!--&lt;!&ndash; Start Single &ndash;&gt;-->
										<!--<div class="dcare__twit d-flex">-->
											<!--<div class="dcare__twit__icon">-->
												<!--<i class="fa fa-twitter"></i>-->
											<!--</div>-->
											<!--<div class="dcare__twit__details">-->
												<!--<p>Lorem ipsum dolor sit  consect ietur adipisicing sed  eiipsa<a href="#"># twitter .com?web/lnk/statement</a></p>-->
												<!--<span><i class="fa fa-clock-o"></i>30th Dec, 2018</span>-->
												<!--<span><i class="fa fa-calendar"></i>30th Dec, 2018</span>-->
											<!--</div>-->
										<!--</div>-->
										<!--&lt;!&ndash; End Single &ndash;&gt;-->
									<!--</div>-->
								<!--</div>-->
							<!--</div>-->
						</div>
						<!-- End Single Widget -->
					</div>
				</div>
				<div class="ft__bottom__images--1 wow flipInX" data-wow-delay="0.6s">
					<img src="/yqyy/public/static/index/images/banner/mid-img/ft.png" alt="footer images">
				</div>
				<div class="ft__bottom__images--2 wow fadeInRight" data-wow-delay="0.6s">
					<img src="/yqyy/public/static/index/images/banner/mid-img/ft-2.png" alt="footer images">
				</div>
			</div>
			<!-- .Start Footer Contact Area -->
			<div class="footer__contact__area bg__cat--2">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="footer__contact__wrapper d-flex flex-wrap justify-content-between">
								<div class="single__footer__address">
									<div class="ft__contact__icon">
										<i class="fa fa-home"></i>
									</div>
									<div class="ft__contact__details">
										<p>新场 冠郡中心</p>
										<p>新场 汇锦城中心</p>
									</div>
								</div>
								<div class="single__footer__address">
									<div class="ft__contact__icon">
										<i class="fa fa-phone"></i>
									</div>
									<div class="ft__contact__details">
										<p><a href="#">13651644074</a></p>
										<p><a href="#">###</a></p>
									</div>
								</div>
								<div class="single__footer__address">
									<div class="ft__contact__icon">
										<i class="fa fa-envelope"></i>
									</div>
									<div class="ft__contact__details">
										<p><a href="#">88354666@qq.com</a></p>
										<p><a href="#">###@yahoo.com</a></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- .End Footer Contact Area -->
			<div class="copyright  bg--theme">
				<div class="container">
					<div class="row align-items-center copyright__wrapper justify-content-center">
						<div class="col-lg-12 col-sm-12 col-md-12">
							<div class="coppy__right__inner text-center">
								<p><i class="fa fa-copyright"></i>Copyright &copy; 2018.Company name All rights reserved.<a target="_blank" href="#">bosn </a></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!-- //Footer Area -->
        <!-- Cartbox -->
        <div class="cartbox-wrap">
            <div class="cartbox text-right">
                <button class="cartbox-close"><i class="zmdi zmdi-close"></i></button>
                <div class="cartbox__inner text-left">
                    <div class="cartbox__items">
                        <!-- Cartbox Single Item -->
                        <div class="cartbox__item">
                            <div class="cartbox__item__thumb">
                                <a href="shop-single.html">
                                    <img src="/yqyy/public/static/index/images/product/sm-pro/1.jpg" alt="small thumbnail">
                                </a>
                            </div>
                            <div class="cartbox__item__content">
                                <h5><a href="shop-single.html" class="product-name">brown jacket</a></h5>
                                <p>Qty: <span>01</span></p>
                                <span class="price">$15</span>
                            </div>
                            <button class="cartbox__item__remove">
                                <i class="fa fa-trash"></i>
                            </button>
                        </div>
                        <!-- //Cartbox Single Item -->
                        <!-- Cartbox Single Item -->
                        <div class="cartbox__item">
                            <div class="cartbox__item__thumb">
                                <a href="shop-single.html">
                                    <img src="/yqyy/public/static/index/images/product/sm-pro/2.jpg" alt="small thumbnail">
                                </a>
                            </div>
                            <div class="cartbox__item__content">
                                <h5><a href="shop-single.html" class="product-name">long sleeve t-shirt</a></h5>
                                <p>Qty: <span>01</span></p>
                                <span class="price">$25</span>
                            </div>
                            <button class="cartbox__item__remove">
                                <i class="fa fa-trash"></i>
                            </button>
                        </div><!-- //Cartbox Single Item -->
                        <!-- Cartbox Single Item -->
                        <div class="cartbox__item">
                            <div class="cartbox__item__thumb">
                                <a href="shop-single.html">
                                    <img src="/yqyy/public/static/index/images/product/sm-pro/3.jpg" alt="small thumbnail">
                                </a>
                            </div>
                            <div class="cartbox__item__content">
                                <h5><a href="shop-single.html" class="product-name">black polo shirt</a></h5>
                                <p>Qty: <span>01</span></p>
                                <span class="price">$30</span>
                            </div>
                            <button class="cartbox__item__remove">
                                <i class="fa fa-trash"></i>
                            </button>
                        </div>
                        <!-- //Cartbox Single Item -->
                    </div>
                    <div class="cartbox__total">
                        <ul>
                            <li><span class="cartbox__total__title">Subtotal</span><span class="price">$70</span></li>
                            <li class="shipping-charge"><span class="cartbox__total__title">Shipping Charge</span><span class="price">$05</span></li>
                            <li class="grandtotal">Total<span class="price">$75</span></li>
                        </ul>
                    </div>
                    <div class="cartbox__buttons">
                        <a class="dcare__btn" href="cart.html"><span>View cart</span></a>
                        <a class="dcare__btn" href="#"><span>Checkout</span></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- //Cartbox -->

        <!-- Register Form -->
        <div class="accountbox-wrapper">
            <div class="accountbox">
                <div class="accountbox__inner">
                	<h4>continue to register</h4>
                    <div class="accountbox__login">
                        <form action="#">
                            <div class="single-input">
                                <input  type="text" placeholder="User name">
                            </div>
                            <div class="single-input">
                                <input type="email" placeholder="E-mail">
                            </div>
                            <div class="single-input">
                                <input type="text" placeholder="Phone">
                            </div>
                            <div class="single-input">
                                <input type="password" placeholder="Password">
                            </div>
                            <div class="single-input">
                                <input type="password" placeholder="Confirm password">
                            </div>
                            <div class="single-input text-center">
                                <button type="submit" class="sign__btn">Sign Up Now</button>
                            </div>
                            <div class="accountbox-login__others text-center">
                                <h6>or register with</h6>
                                <ul class="dacre__social__link d-flex justify-content-center">
                                    <li class="facebook"><a target="_blank" href="#"><span class="ti-facebook"></span></a></li>
                                    <li class="twitter"><a target="_blank" href="#"><span class="ti-twitter"></span></a></li>
                                    <li class="pinterest"><a target="_blank" href="#"><span class="ti-google"></span></a></li>
                                </ul>
                            </div>
                        </form>
                    </div>
                    <span class="accountbox-close-button"><i class="zmdi zmdi-close"></i></span>
                </div>
                <h3>Have an account ? Login Fast</h3>
            </div>
        </div><!-- //Register Form -->

        <!-- Login Form -->
        <div class="login-wrapper">
            <div class="accountbox">
                <div class="accountbox__inner">
                	<h4>Login to Continue</h4>
                    <div class="accountbox__login">
                        <form action="#">
                            <div class="single-input">
                                <input type="email" placeholder="E-mail">
                            </div>
                            <div class="single-input">
                                <input type="password" placeholder="Password">
                            </div>
                            <div class="single-input text-center">
                                <button type="submit" class="sign__btn">SUBMIT</button>
                            </div>
                            <div class="accountbox-login__others text-center">
                                <ul class="dacre__social__link d-flex justify-content-center">
                                    <li class="facebook"><a target="_blank" href="#"><span class="ti-facebook"></span></a></li>
                                    <li class="twitter"><a target="_blank" href="#"><span class="ti-twitter"></span></a></li>
                                    <li class="pinterest"><a target="_blank" href="#"><span class="ti-google"></span></a></li>
                                </ul>
                            </div>
                        </form>
                    </div>
                    <span class="accountbox-close-button"><i class="zmdi zmdi-close"></i></span>
                </div>
                <h3>Have an account ? Login Fast</h3>
            </div>
        </div><!-- //Login Form -->

	</div><!-- //Main wrapper -->

	<!-- JS Files -->
	<script src="/yqyy/public/static/index/js/vendor/jquery-3.2.1.min.js"></script>
	<script src="/yqyy/public/static/index/js/popper.min.js"></script>
	<script src="/yqyy/public/static/index/js/bootstrap.min.js"></script>
	<script src="/yqyy/public/static/index/js/plugins.js"></script>
	<script src="/yqyy/public/static/index/js/active.js"></script>
</body>
</html>

